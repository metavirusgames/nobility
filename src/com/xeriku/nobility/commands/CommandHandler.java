package com.xeriku.nobility.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.xeriku.nobility.Bounty;
import com.xeriku.nobility.Guild;
import com.xeriku.nobility.NobilityPlugin;
import com.xeriku.nobility.RankType;

public class CommandHandler {

	private List<Guild> _guilds = new ArrayList<Guild>();
	private NobilityPlugin plugin;
	
	public CommandHandler(NobilityPlugin plugin){
		this.plugin = plugin;
	}
	
    public boolean Handle(CommandSender sender, Command cmd, String label, String[] args){
		
    	if (cmd.getName().equalsIgnoreCase("n")){
    		
    		if (args.length > 0){
    			if (sender instanceof Player){
	    			Player player = (Player)sender;
	    			Guild guild = this.plugin.getGuildManager().getGuildByPlayer(player);
	    			
	    			if (args[0].equalsIgnoreCase("CREATE")){
	    				if (guild == null){
	    					this.plugin.getLogger().info("Attempting to create a guild: " + args[1]);
	    					this.plugin.getGuildManager().create(player, args[1]);
	    				}else{
	    					player.sendMessage("You cannot create a guild, you are already in: " + ChatColor.GOLD + "[" + guild.getName() + "]");
	    					player.sendMessage("You must leave your current guild or disband it!");
	    				}
	    				
	    			}else if (args[0].equalsIgnoreCase("DISBAND")){
	    				
	    				if (guild != null){
	    					guild.broadCastToGuild(this.plugin, "Your guild was disbanded");
	    					this.plugin.getGuildManager().disband(player, guild.getName());
	    					this.plugin.getPluginConfig().delete(guild);
	    				}
	    				
	    			}else if (args[0].equalsIgnoreCase("MOTTO")){
	    				
	    				if (guild != null){
	    					String motto = "";
	    					
	    					//Construct our motto string
	    					for (int x = 1; x < args.length; x++){
	    						motto += " " + args[x];
	    					}
	    					
	    					guild.setMotto(motto);
	    					
	    					this.plugin.getGuildManager().udpateGuild(player, guild);
	    					this.plugin.getPluginConfig().save(guild);
	    					
	    					player.sendMessage("Set the motto of your guild to: " + motto);
	    				}
	    				
	    			}else if (args[0].equalsIgnoreCase("HOME")){
	    				try {
							if (guild != null){
		    					if (!guild.getHome().isEmpty()){
		    						String[] homeCoords = guild.getHome().split(",");
		    						
		    						if (homeCoords != null && homeCoords.length == 3){
		    							player.teleport(new Location(player.getWorld(), Double.valueOf(homeCoords[0]), Double.valueOf(homeCoords[1]), Double.valueOf(homeCoords[2])));
		    						}else {
		    							this.plugin.getLogger().info("Non-Valid Home Was Found!");
		    						}
		    					}else {
									player.sendMessage(ChatColor.RED + "You have not yet set a home for your guild!");
								}
							}
						} catch (Exception e) {
							this.plugin.getLogger().info(e.toString());
						}
	    				
	    			}
	    			else if (args[0].equalsIgnoreCase("SETHOME")){
	    				if (guild != null){
	    					player.sendMessage("Set guild home!");
	    					guild.setHome((int)player.getLocation().getX() + "," + (int)player.getLocation().getY() + "," + (int)player.getLocation().getZ());
	    					this.plugin.getPluginConfig().save(guild);
	    				}
	    				
	    			}else if (args[0].equalsIgnoreCase("INFO")){
	    				if (guild != null){
	    					
	    				}
	    				
	    			}else if (args[0].equalsIgnoreCase("CLAIM")){
	    				if (guild != null){
	    				Location location = player.getLocation();
	    				int x, y, z;
	    				
	    				x = (int)location.getX() / 16;
	    				y = (int)location.getY() / 256;
	    				z = (int)location.getZ() / 16;
	    				
		    				if (!guild.ownsClaim(x, z) && this.plugin.getGuildManager().getLandOwner(x, z) == null){
		    					
		    					
		    					if (guild.getTotalCurrentClaims() < guild.getTotalAllowedClaims()){
		    						player.sendMessage(String.format("Claimed land at {%s, %s, %s}", x, y, z));
		    						guild.claim(x, z);
		    						this.plugin.getPluginConfig().save(guild);
		    					}else{
		    						player.sendMessage(ChatColor.RED + "You do not have anymore available claims at this time...");
		    					}
		    					
		    				}else if (this.plugin.getGuildManager().getLandOwner(x, z) != null){
		    					Guild ownerGuild = this.plugin.getGuildManager().getLandOwner(x, z);
		    					
		    					if (ownerGuild != null){
		    						if (ownerGuild.isGuildEnemy(guild.getName())){
		    							player.sendMessage(ChatColor.RED + "You cannot claim this land, this land is owned by another guild and they are strong enough to keep it");
		    						}else{
		    							player.sendMessage(ChatColor.GOLD + "You cannot claim the land of an ally or neutral guild.");
		    						}
		    					}
		    					
		    				}else{
		    					player.sendMessage("Your guild already owns this land");
		    				}
	    				}else{
	    					player.sendMessage("You are not in a guild, you must be in a guild to claim land!");
	    				}
	    			}else if (args[0].equalsIgnoreCase("UNCLAIM")){
	    				if (guild != null){
		    				Location location = player.getLocation();
		    				int x, y, z;
		    				
		    				x = (int)location.getX() / 16;
		    				y = (int)location.getY() / 256;
		    				z = (int)location.getZ() / 16;
		    				
			    				if (guild.ownsClaim(x, z)){
			    					player.sendMessage(String.format("Unclaimed land at {%s, %s, %s}", x, y, z));
			    					guild.unclaim(x, z);
			    					this.plugin.getPluginConfig().save(guild);
			    				}else{
			    					player.sendMessage("You cannot unclaim land you do not own");
			    				}
		    				}else{
		    					player.sendMessage("You are not in a guild, you must be in a guild to unclaim land!");
		    				}
	    			
	    			}else if (args[0].equalsIgnoreCase("CLAIMS")){
	    				
	    				if (guild != null){
	    					player.sendMessage(ChatColor.GOLD + "Claims: " + guild.getTotalCurrentClaims() + " / " + guild.getTotalAllowedClaims());
	    				}
	    				
	    			}else if (args[0].equalsIgnoreCase("MAP")){
	    				if (guild != null){
	    					guild.showMap(player);
	    				}
	    			}else if (args[0].equalsIgnoreCase("INVITE")){
	    				
	    				if (guild != null){
		    				if (args.length > 1){
		    					Player invitee = this.plugin.getServer().getPlayer(args[1]);
		    					
		    					if (invitee != null){
		    						if (invitee.isOnline()){
		    							invitee.sendMessage(ChatColor.GOLD + player.getName() + " has invited you to " + guild.getName());
		    							invitee.sendMessage(ChatColor.GOLD + "Type /n join " + guild.getName() + " to join");
		    							guild.invite(invitee.getName());
		    						}else{
		    							player.sendMessage(ChatColor.RED + "The player is not online right now. Send them an invite when they are online again...");
		    						}
		    					}else{
		    						player.sendMessage(ChatColor.RED + "Player not found...");
		    					}
		    				}
	    				}
	    			}//else if (args[0].equalsIgnoreCase("KICK")){
//	    				if (guild != null){
//	    					if (args.length > 1){
//	    						if ((guild.isPlayerOfficer(player.getName()) || guild.isPlayerGuildMaster(player.getName())) && guild.isPlayerInGuild(args[1])){
//	    							guild.kick(args[1]);
//	    							this.plugin.getPluginConfig().save(guild);
//	    							Player kickedPlayer = this.plugin.getServer().getPlayer(args[1]);
//	    							
//	    							if (kickedPlayer != null && kickedPlayer.isOnline()){
//	    								kickedPlayer.sendMessage(ChatColor.RED + "You were kicked out of " + guild.getName());
//	    							}
//	    							
//	    							guild.broadCastToGuild(this.plugin, ChatColor.GOLD + args[1] + " was kicked out of the guild!");
//	    						}
//	    					}
//	    				}else{
//	    					player.sendMessage("You must be in a guild and an officer or leader of the guild to kick someone!");
//	    				}
//	    				
	    			else if (args[0].equalsIgnoreCase("JOIN")){
	    				
	    				if (guild == null){
	    					
	    					if (args.length > 1){
	    						Guild inviteeGuild = this.plugin.getGuildManager().getGuild(args[1]);
	    						
	    						if (inviteeGuild != null){
	    							inviteeGuild.join(player);
	    							this.plugin.getPluginConfig().save(inviteeGuild);
	    							
	    							for (Entry<String, Object> guildPlayer : inviteeGuild.getPlayers().entrySet()){
	    								if (guildPlayer != null){
	    									Player guildPlayerObject = this.plugin.getServer().getPlayer(guildPlayer.getKey());
	    									
	    									if (guildPlayerObject != null && guildPlayerObject.isOnline()){
	    										guildPlayerObject.sendMessage(player.getName() + " has joined " + inviteeGuild.getName());
	    									}
	    								}
	    							}
	    						}else{
	    							player.sendMessage("Could not find the guild " + args[1]);
	    						}
	    					}
	    				}else{
	    					player.sendMessage("You are not part of a guild.");
	    				}
	    			}else if (args[0].equalsIgnoreCase("KICK")){
	    				
	    				this.plugin.getLogger().info("Attempting to kick player " + args[1] + " from " + guild.getName());
	    				
	    				if (guild != null && args.length > 1){
	    					this.plugin.getLogger().info("Kicking player is in a guild, attempting kick");
	    					
	    					Player playerToKick = this.plugin.getServer().getPlayer(args[1]);
	    						    				
	    					if (guild.isPlayerInGuild(playerToKick.getName()) && (guild.isPlayerOfficer(player.getName()) || guild.isPlayerGuildMaster(player.getName()))){
	    						this.plugin.getLogger().info(args[1] + " was kicked from " + guild.getName());
	    						guild.kick(args[1]);
								this.plugin.getPluginConfig().save(guild);
								
								if (playerToKick != null){
	    							if (playerToKick.isOnline()){
	    								playerToKick.sendMessage(ChatColor.RED + "You were kicked out of " + guild.getName());
	    								
	    							}
		    					}
	    					}
	    					
	    					for (Entry<String, Object> guildPlayer : guild.getPlayers().entrySet()){
								if (guildPlayer != null){
									Player guildPlayerObject = this.plugin.getServer().getPlayer(guildPlayer.getKey());
									
									if (guildPlayerObject != null && guildPlayerObject.isOnline()){
										guildPlayerObject.sendMessage(ChatColor.RED + args[1] + " was kicked from the guild.");
									}
								}
							}
	    				}
	    			}else if (args[0].equalsIgnoreCase("ALLY")){
	    				if (guild != null && args.length > 1){
    						Guild allyInviteGuild = this.plugin.getGuildManager().getGuild(args[1]);
    						
    						if (allyInviteGuild != null){
    							
    							if (!guild.getName().equalsIgnoreCase(allyInviteGuild.getName()) && !guild.getAllyInvites().contains(args[1]) && !guild.isGuildAlly(args[1]) && !allyInviteGuild.getAllyInvites().contains(args[1])){
    								guild.ally(args[1]);
    								allyInviteGuild.broadCastToGuild(this.plugin, ChatColor.AQUA + guild.getName() + " invited you to be an ally. Type /n ally " + guild.getName() + " to accept.");
    								guild.broadCastToGuild(this.plugin, ChatColor.GOLD + "Your guild has invited " + allyInviteGuild.getName() + " to form an alliance!");
    							}else if (allyInviteGuild.getAllyInvites().contains(guild.getName())) {
    								guild.addAlly(allyInviteGuild.getName());
    								guild.broadCastToGuild(this.plugin, "Your guild is now allied with " + allyInviteGuild.getName());
    								allyInviteGuild.addAlly(guild.getName());
    								allyInviteGuild.broadCastToGuild(this.plugin, "Your guild is now allied with " + guild.getName());
    								this.plugin.getPluginConfig().save(allyInviteGuild);
    								this.plugin.getPluginConfig().save(guild);
    							}
    							
    							if (guild.getName().equalsIgnoreCase(allyInviteGuild.getName())){
    								player.sendMessage(ChatColor.RED + "You cannot ally your own guild...");
    							}
    						}
	    				}	    				
	    			}else if (args[0].equalsIgnoreCase("NEUTRAL")){
	    				if (guild != null && args.length > 1){
    						Guild neutralInviteGuild = this.plugin.getGuildManager().getGuild(args[1]);
    						
    						if (neutralInviteGuild != null){
    							
    							if (!guild.getName().equalsIgnoreCase(neutralInviteGuild.getName()) && !guild.getNeutralInvites().contains(args[1]) && !guild.isGuildNeutral(args[1]) && !neutralInviteGuild.getNeutralInvites().contains(args[1])){
    								guild.neutral(args[1]);
    								neutralInviteGuild.broadCastToGuild(this.plugin, ChatColor.AQUA + guild.getName() + " invited you to be neutral. Type /n neutral " + guild.getName() + " to accept.");
    								guild.broadCastToGuild(this.plugin, ChatColor.GOLD + "Your guild has invited " + neutralInviteGuild.getName() + " to become neutral!");
    							}else if (neutralInviteGuild.getNeutralInvites().contains(guild.getName())) {
    								guild.addNeutral(neutralInviteGuild.getName());
    								guild.broadCastToGuild(this.plugin, "Your guild is now neutral with " + neutralInviteGuild.getName());
    								neutralInviteGuild.addNeutral(guild.getName());
    								neutralInviteGuild.broadCastToGuild(this.plugin, "Your guild is now neutral with " + guild.getName());
    								this.plugin.getPluginConfig().save(neutralInviteGuild);
    								this.plugin.getPluginConfig().save(guild);
    							}
    							
    							if (guild.getName().equalsIgnoreCase(neutralInviteGuild.getName())){
    								player.sendMessage(ChatColor.RED + "You cannot neutral your own guild...");
    							}
    						}
	    				}	    	
		    		}else if (args[0].equalsIgnoreCase("ENEMY")){
	    				
	    				if (guild != null){
		    				if (args.length > 1){
		    					Guild guildToEnemy = this.plugin.getGuildManager().getGuild(args[1]);
		    					this.plugin.getLogger().info("Enemy guild found: " + guildToEnemy.getName());
		    					
		    					if (guildToEnemy != null){
		    						
		    						if (guildToEnemy.getEnemies() == null){
		    							this.plugin.getLogger().info("Enemies list is null");
		    						}
		    						
		    						if (guild.getEnemies() == null){
		    							this.plugin.getLogger().info("Guild enemy list is null");
		    						}
		    						
		    						if (!guild.getName().equalsIgnoreCase(guildToEnemy.getName())){
		    							
		    							if (!guildToEnemy.getEnemies().contains(guild.getName()) && !guild.getEnemies().contains(args[1])) {
			    							this.plugin.getLogger().info(guild.getName() + " is attempting to enemy " + guildToEnemy.getName());
			    							
			    							try {
												guildToEnemy.addEnemy(guild.getName());
												guildToEnemy.broadCastToGuild(this.plugin, ChatColor.RED + "You are now enemies with " + guild.getName());
												guild.addEnemy(guildToEnemy.getName());
												guild.broadCastToGuild(this.plugin, ChatColor.RED + "You are now enemies with " + guildToEnemy.getName());
			    							
												this.plugin.getPluginConfig().save(guild);
												this.plugin.getPluginConfig().save(guildToEnemy);
											} catch (Exception e) {
												this.plugin.getLogger().info(e.toString());
											}
		    							}else{
			    							player.sendMessage("You are already enemies with this faction!");
			    						}
		    							
		    						}else{
		    							player.sendMessage(ChatColor.RED + "You cannot enemy your own guild...");
		    						}
		    						
		    					}else{
		    						player.sendMessage("The guild you are trying to enemy does not exist");
		    					}
		    				}	
	    				}else{
	    					player.sendMessage(ChatColor.RED + "You are not in a guild!");
	    				}
	    			}else if (args[0].equalsIgnoreCase("BOUNTY")){
	    				if (args.length > 2){
    						Bounty bounty = new Bounty();
    						
    						bounty.setPlayer(args[1]);
    						bounty.setAmount(Integer.valueOf(args[2]));
    						player.sendMessage("You set a bounty on the head of " + args[1]);
    						this.plugin.getPluginConfig().save(bounty);
	    					
	    				}
	    			}else if (args[0].equalsIgnoreCase("BOUNTIES")){
	    				
	    				int j = 0;
	    				String line = "";
	    				player.sendMessage("==Current Bounties==");
	    				
	    				for (Bounty bounty : this.plugin.getBountyManager().getBounties()){
	    					if (bounty != null){
	    						player.sendMessage(ChatColor.GOLD + "Player: " + bounty.getPlayer() + ", Amount: " + bounty.getAmount());
	    					}
	    				}
	    			}
	    			
    			}	
    			
    			return true;
    		}
    		
    		return true;
    	}
    	
    	return false;
	}
		
	public List<Guild> getGuilds(){
		return this._guilds;
	}
}

package com.xeriku.nobility.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.xeriku.nobility.Guild;
import com.xeriku.nobility.NobilityPlugin;

public class MoveEventListener implements Listener {

	private NobilityPlugin plugin;
	private Map<String, String> previousLandCache;
	
	public MoveEventListener(NobilityPlugin plugin){
		this.previousLandCache = new HashMap<String, String>();
		
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled=true)
	public void onPlayerMove(PlayerMoveEvent event){
		
		try {
			Guild guild = this.plugin.getGuildManager().getGuildByPlayer(event.getPlayer());
			
			if (guild != null){
			
			Location location = event.getPlayer().getLocation();
			int x, y, z;
			
			x = (int)location.getX() / 16;
			y = (int)location.getY() / 256;
			z = (int)location.getZ() / 16;
			
			boolean guildOwns = false;
			
			try{
				guildOwns = guild.ownsClaim(x, z);
			}catch (Exception e){
				this.plugin.getLogger().info("An exception occurred while trying to check the land claims");
				this.plugin.getLogger().info(e.toString());
			}
			
			if (guildOwns){
				if (getLastLand(event.getPlayer()) != "GUILD"){
					if (guild.getMotto() != null){
						event.getPlayer().sendMessage(String.format(ChatColor.GOLD + "You have entered %s territory, %s", guild.getName(), guild.getMotto()));
					}else{
						event.getPlayer().sendMessage(String.format(ChatColor.GOLD + "You have entered %s territory", guild.getName()));
					}
					event.getPlayer().sendMessage("Land health is " + guild.getLandHealth(x, z));
					
					this.previousLandCache.put(event.getPlayer().getName(), "GUILD");
				}
				
			/* If the player guild does not own this land, display information about who does own it */
			}else if (this.plugin.getGuildManager().getLandOwner(x, z) != null){
				Guild landOwner = this.plugin.getGuildManager().getLandOwner(x, z);
				
				/* Enemy Land Handling */
				if (landOwner.isGuildEnemy(guild.getName()) && getLastLand(event.getPlayer()) != "ENEMY"){
					if (landOwner.getMotto() != null){
						event.getPlayer().sendMessage(String.format(ChatColor.RED + "You have entered %s territory, %s", landOwner.getName(), landOwner.getMotto()));
					}else{
						event.getPlayer().sendMessage(String.format(ChatColor.RED + "You have entered %s territory", landOwner.getName()));
					}
					event.getPlayer().sendMessage("Land health is " + landOwner.getLandHealth(x, z));
					this.previousLandCache.put(event.getPlayer().getName(), "ENEMY");
								
					
					
				/* Ally Land Handling */
				} else if (landOwner.isGuildAlly(guild.getName()) && getLastLand(event.getPlayer()) != "ALLY"){
					if (landOwner.getMotto() != null){
						event.getPlayer().sendMessage(String.format(ChatColor.GREEN + "You have entered %s territory, %s", landOwner.getName(), landOwner.getMotto()));
					}else{
						event.getPlayer().sendMessage(String.format(ChatColor.GREEN + "You have entered %s territory", landOwner.getName()));
					}
					event.getPlayer().sendMessage("Land health is " + landOwner.getLandHealth(x, z));
					this.previousLandCache.put(event.getPlayer().getName(), "ALLY");
					
				/* Neutral Land Handling */
				}else if (landOwner.isGuildNeutral(guild.getName()) && getLastLand(event.getPlayer()) != "NEUTRAL"){
					if (landOwner.getMotto() != null){
						event.getPlayer().sendMessage(String.format(ChatColor.BLUE + "You have entered %s territory, %s", landOwner.getName(), landOwner.getMotto()));
					}else{
						event.getPlayer().sendMessage(String.format(ChatColor.BLUE + "You have entered %s territory", landOwner.getName()));
					}
					event.getPlayer().sendMessage("Land health is " + landOwner.getLandHealth(x, z));
					this.previousLandCache.put(event.getPlayer().getName(), "NEUTRAL");
					
				}else if (!landOwner.isGuildNeutral(guild.getName()) && !landOwner.isGuildEnemy(guild.getName()) && !landOwner.isGuildAlly(guild.getName())){
					 if (getLastLand(event.getPlayer()) != "NONE"){
						if (landOwner.getMotto() != null){
							event.getPlayer().sendMessage(String.format(ChatColor.GRAY + "You have entered %s territory, %s", landOwner.getName(), landOwner.getMotto()));
						}else{
							event.getPlayer().sendMessage(String.format(ChatColor.GRAY + "You have entered %s territory", landOwner.getName()));
						}
						
						this.previousLandCache.put(event.getPlayer().getName(), "NONE");
					 }
				}
				
			}else{
				if (getLastLand(event.getPlayer()) != "WILD"){
					event.getPlayer().sendMessage(ChatColor.DARK_GREEN + "You have entered the wild..");
					this.previousLandCache.put(event.getPlayer().getName(), "WILD");
					
					//guild.cancelBattles();
				}
			}
		}else{
			if (getLastLand(event.getPlayer()) != "WILD"){
				event.getPlayer().sendMessage(ChatColor.DARK_GREEN + "You have entered the wild..");
				this.previousLandCache.put(event.getPlayer().getName(), "WILD");
				
				//guild.cancelBattles();
			}
		}
		} catch (Exception e) {
			this.plugin.getLogger().info(e.getStackTrace().toString());
		}
		
		
		
		
	}
	
	private String getLastLand(Player player){
		
		if (this.previousLandCache.containsKey(player.getName())){
			return this.previousLandCache.get(player.getName()).toUpperCase();
		}
		
		return "";
	}
}

package com.xeriku.nobility.listeners;

import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.xeriku.nobility.Guild;
import com.xeriku.nobility.NobilityPlugin;
import com.xeriku.nobility.RankType;

public class ChatListener implements Listener {

	private NobilityPlugin plugin;
	
	public ChatListener(NobilityPlugin plugin){
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled=true)
	public void onPlayerChat(AsyncPlayerChatEvent event){
		Guild guild = this.plugin.getGuildManager().getGuildByPlayer(event.getPlayer());
		
		if (guild != null){
			
			Entry<String, Object> entry = guild.getPlayerEntry(event.getPlayer());
			
			if (entry != null && entry.getValue().equals(RankType.GuildMaster)){
				event.setFormat(ChatColor.GOLD + "[{" + guild.getName() + "}]" + ChatColor.WHITE + event.getFormat());
			}else if (entry != null && entry.getValue().equals(RankType.Officer)){
				event.setFormat(ChatColor.DARK_AQUA + "[" + guild.getName() + "]" + ChatColor.WHITE + event.getFormat());
			}else{
				event.setFormat(ChatColor.GREEN + "[" + guild.getName() + "]" + ChatColor.WHITE + event.getFormat());
			}
		}
	}
}

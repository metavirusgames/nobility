package com.xeriku.nobility.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import com.xeriku.nobility.Guild;
import com.xeriku.nobility.NobilityPlugin;

public class BlockChangeListener implements Listener {

private NobilityPlugin plugin;
	
	public BlockChangeListener(NobilityPlugin plugin){
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled=false)
	public void onBlockDamage(BlockDamageEvent event){
		
		Guild guild = this.plugin.getGuildManager().getGuildByPlayer(event.getPlayer());
		Guild landOwner = this.plugin.getGuildManager().getLandOwner((int)(event.getBlock().getLocation().getX() / 16), (int)(event.getBlock().getLocation().getZ() / 16));
		
		if (landOwner != null){
			
			if (guild == null){
				event.getPlayer().sendMessage("This land is claimed by " + landOwner.getName() + ", you may not damage it");
				event.setCancelled(true);
			}else {
				//If you aren't the in the guild owning the land
				if (!guild.getName().equalsIgnoreCase(landOwner.getName())){
					
					event.getPlayer().sendMessage("This land is claimed by " + landOwner.getName() + ", you may not damage it");
					event.setCancelled(true);
				}
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled=false)
	public void onBlockBreak(BlockBreakEvent event){
		
		Guild guild = this.plugin.getGuildManager().getGuildByPlayer(event.getPlayer());
		Guild landOwner = this.plugin.getGuildManager().getLandOwner((int)(event.getBlock().getLocation().getX() / 16), (int)(event.getBlock().getLocation().getZ() / 16));
		
		if (landOwner != null){
			
			if (guild == null){
				event.getPlayer().sendMessage("This land is claimed by " + landOwner.getName() + ", you may not damage it");
				event.setCancelled(true);
			}else {
				//If you aren't the in the guild owning the land
				if (!guild.getName().equalsIgnoreCase(landOwner.getName())){
					event.getPlayer().sendMessage("This land is claimed by " + landOwner.getName() + ", you may not damage it");
					event.setCancelled(true);
				}
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled=false)
	public void onBlockPlace(BlockPlaceEvent event){
		
		Guild guild = this.plugin.getGuildManager().getGuildByPlayer(event.getPlayer());
		Guild landOwner = this.plugin.getGuildManager().getLandOwner((int)(event.getBlock().getLocation().getX() / 16), (int)(event.getBlock().getLocation().getZ() / 16));
		
		if (landOwner != null){
			
			if (guild == null){
				event.getPlayer().sendMessage("This land is claimed by " + landOwner.getName() + ", you may not build on it");
				event.setCancelled(true);
			}else {
				//If you aren't the in the guild owning the land
				if (!guild.getName().equalsIgnoreCase(landOwner.getName())){
					event.getPlayer().sendMessage("This land is claimed by " + landOwner.getName() + ", you may not build on it");
					event.setCancelled(true);
				}
			}
		}
	}
}

package com.xeriku.nobility.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

import com.xeriku.nobility.Guild;
import com.xeriku.nobility.NobilityPlugin;
import com.xeriku.nobility.RankType;

public class GuildManager {
	
	private List<Guild> _guilds;
	
	private NobilityPlugin plugin;
	
	public GuildManager(NobilityPlugin plugin){
		this.plugin = plugin;
		
		_guilds = new ArrayList<Guild>();
		
	}
	
	public void loadGuilds(){
		this.plugin.getLogger().info("Attempting to load guilds!");
		File folder = new File("plugins/nobility/.guilds/");
		File[] files = folder.listFiles();
		
		this.plugin.getLogger().info(files.length + " total guilds found...");
		
		for (int i = 0; i < files.length; i++){
			if (files[i].isFile()){
				String filename = files[i].getName();
				
				if (filename.endsWith(".guild")){
					this.plugin.getLogger().info("Loading guild: " + filename);
					this._guilds.add(this.plugin.getPluginConfig().loadGuild(filename));
				}
			}
		}
	}
	
	public void saveGuilds(){
		
	}
	
	public void create(Player playerExecuting, String name){
		Guild guild = new Guild();
		
		guild.setName(name);
		guild.addPlayer(playerExecuting, RankType.GuildMaster);
		
		_guilds.add(guild);
		
		playerExecuting.sendMessage("Your guild " + name + " has been created!");
		
		this.plugin.getPluginConfig().save(guild);
	}
	
	public boolean exists(Player playerExecuting, String name){
		
		for (Guild guild : this._guilds){
			if (guild.getName().equalsIgnoreCase(name)){
				return true;
			}
		}
		
		return false;
	}
	
	public void disband(Player playerExecuting, String name){
		Guild guild = getGuildByPlayer(playerExecuting);
		
		if (guild != null){
			if (guild.isPlayerOfficer(playerExecuting.getName()) || guild.isPlayerGuildMaster(playerExecuting.getName())){
				for (Entry<String, Object> playerEntry : guild.getPlayers().entrySet()){
					
					if (playerEntry != null){
						guild.kick(playerEntry.getKey());
					}
				}
			}
			
			this._guilds.remove(guild);
		}
	}
	
	public void addPlayerToGuild(Player playerExecuting, String playerName){
		
	}
	
	public void removePlayer(Player playerExecuting, String name){
		
		Guild guild = getGuildByPlayer(playerExecuting);
		
		if (guild != null){
			if (guild.isPlayerOfficer(playerExecuting.getName()) || guild.isPlayerGuildMaster(playerExecuting.getName())){
				if (guild.isPlayerInGuild(name)){
					guild.kick(name);
				}
			}
		}
	}
	
	public void playerExists(String name){
		
	}
	
	public void udpateGuild(Player playerExecuting, Guild guild){
		
		for (Guild item : this._guilds){
			if (item.getName().equalsIgnoreCase(guild.getName())){
				item = guild;
			}
		}
	}
	
	public Guild getLandOwner(int x, int z){
		
		for (Guild guildToCheck : this._guilds){
			if (guildToCheck != null){
				
				if (guildToCheck.ownsClaim(x, z)){
					return guildToCheck;
				}
			}
		}
		
		return null;
	}
	
	public List<Guild> getGuilds(){
		return _guilds;
	}
	
	public Guild getGuild(String name){
		
		for (Guild guild : this._guilds){
			if (guild.getName().equalsIgnoreCase(name)){
				return guild;
			}
		}
		
		return null;
	}
	
	public Guild getGuildByPlayer(Player player){
		
		for (Guild guild : this._guilds){
			for (Entry<String, Object> entry : guild.getPlayers().entrySet()){
				
				if (entry != null){
					if (entry.getKey().equalsIgnoreCase(player.getName())){
						return guild;
					}
				}
			}
		}
		
		return null;
	}
}

package com.xeriku.nobility.core;

import java.io.File;

public class Paths {

	public static File getPluginFolder(){
		return new File("plugins/nobility");
	}
	
	public static File getGuildsFolder(){
		return new File("plugins/nobility/.guilds");
	}
	
	public static File getBountiesFolder(){
		return new File("plugins/nobility/.bounties");
	}
	
	public static File getKingdomsFolder(){
		return new File("plugins/nobility/.kingdoms");
	}
	
	public static File getConfigFileName(){
		return new File("plugins/nobility/config.yml");
	}
	
	public static void Check(){
		if (!Paths.getPluginFolder().exists()){
			Paths.getPluginFolder().mkdir();
		}
		
		if (!Paths.getGuildsFolder().exists()){
			Paths.getGuildsFolder().mkdir();
		}
		
		if (!Paths.getKingdomsFolder().exists()){
			Paths.getKingdomsFolder().mkdir();
		}
		
		if (!Paths.getConfigFileName().exists()){
			Paths.getConfigFileName().mkdir();
		}
		
		if (!Paths.getBountiesFolder().exists()){
			Paths.getBountiesFolder().mkdir();
		}
	}
}

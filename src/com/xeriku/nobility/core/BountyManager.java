package com.xeriku.nobility.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.xeriku.nobility.Bounty;
import com.xeriku.nobility.NobilityPlugin;

public class BountyManager {

	private List<Bounty> _bounties;
	
	private NobilityPlugin plugin;
	
	public BountyManager(NobilityPlugin plugin){
		this._bounties = new ArrayList<Bounty>();
		this.plugin = plugin;
	}
	
	public void loadBounties(){
		this.plugin.getLogger().info("Attempting to load bounties!");
		File folder = new File("plugins/nobility/.bounties/");
		File[] files = folder.listFiles();
		
		this.plugin.getLogger().info(files.length + " total bounties found...");
		
		for (int i = 0; i < files.length; i++){
			if (files[i].isFile()){
				String filename = files[i].getName();
				
				if (filename.endsWith(".bounty")){
					this.plugin.getLogger().info("Loading bounty: " + filename);
					this._bounties.add(this.plugin.getPluginConfig().loadBounty(filename));
				}
			}
		}
	}
	
	public void setBounties(List<Bounty> map){
		this._bounties = map;
	}
	
	public List<Bounty> getBounties(){
		return this._bounties;
	}
	
	public void addBounty(Bounty bounty){
		this._bounties.add(bounty);
	}
	
	public void removeBounty(Bounty bounty){
		this._bounties.remove(bounty);
	}
}

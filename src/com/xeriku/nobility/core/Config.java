package com.xeriku.nobility.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.google.common.collect.Multiset.Entry;
import com.xeriku.nobility.Bounty;
import com.xeriku.nobility.Guild;
import com.xeriku.nobility.NobilityPlugin;

public class Config {

	private NobilityPlugin plugin;
	
	public Config(NobilityPlugin plugin){
		this.plugin = plugin;
	}
	
	public Bounty loadBounty(String filename){
		
		try{
		File file = new File("plugins/nobility/.bounties/" + filename);
		Bounty bounty = new Bounty();
		YamlConfiguration config;
		
		config = YamlConfiguration.loadConfiguration(file);
		
		bounty.setPlayer(config.getString("Player"));
		bounty.setAmount(config.getInt("Amount"));
		
		return bounty;
		}catch (Exception ex){
			this.plugin.getLogger().info(ex.toString());
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Guild loadGuild(String filename){
		
		try{
			this.plugin.getLogger().info("Attempting to load guild file: " + filename);
			File file = new File("plugins/nobility/.guilds/" + filename);
			//FileConfiguration config;
			YamlConfiguration config;
			
			config = YamlConfiguration.loadConfiguration(file);
			
			Guild guild = new Guild();
			
			guild.setName(config.getString("Name"));
			this.plugin.getLogger().info("Guild Name: " + config.getString("Name"));
			
			if (config.getString("Motto") != null){
				guild.setMotto(config.getString("Motto"));
				this.plugin.getLogger().info("Guild Motto: " + config.getString("Motto"));
			}
			
			
			
			MemorySection memberMap = (MemorySection) config.get("Members");
			MemorySection landHealthMap = (MemorySection) config.get("LandHealth");
			
			List<String> landMap = (List<String>) config.getList("Land");
			
			guild.setMembers(memberMap.getValues(true));
			guild.setLand(landMap);
			guild.setLandHealth(landHealthMap.getValues(true));
			guild.setEnemies((List<String>)config.getList("Enemies"));
			guild.setAllies((List<String>)config.getList("Allies"));
			guild.setNeutrals((List<String>)config.getList("Neutrals"));
			
			String homeCoords = config.getString("Home");
			guild.setHome(homeCoords);
			
//			if (homeCoords != null && homeCoords.length == 3){
//				guild.setHome(Integer.getInteger(homeCoords[0]), Integer.getInteger(homeCoords[1]), Integer.getInteger(homeCoords[2]));
//			}else {
//				this.plugin.getLogger().info("Non-Valid Home Was Found!");
//			}
			
			this.plugin.getLogger().info(filename + " was loaded successfully!");
			
			return guild;		
		}catch (Exception ex){
			this.plugin.getLogger().info("An exception occurred while attempting to load the guild file: " + filename);
			this.plugin.getLogger().info(ex.toString());
			return null;
		}
		

	}
	
	public void delete(Guild guild){
		File file = new File("plugins/nobility/.guilds", guild.getName() + ".guild");
		
		if (file.exists()){
			file.delete();
		}
	}
	
	public void save(Guild guild){
		File file = new File("plugins/nobility/.guilds", guild.getName() + ".guild");
		FileConfiguration config;
		
		config = YamlConfiguration.loadConfiguration(file);
		
		config.set("Name", guild.getName());
		config.set("Motto", guild.getMotto());
		
		config.set("Members", guild.getPlayers());
		config.set("Land", guild.getLand());
		config.set("LandHealth", guild.getLandHealth());
		config.set("Enemies", guild.getEnemies());
		config.set("Allies", guild.getAllies());
		config.set("Neutrals", guild.getNeutrals());
		config.set("Home", guild.getHome());
		try {
			config.save(file);
			this.plugin.getLogger().info("Guild saved!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void save(Bounty bounty){
		File file = new File("plugins/nobility/.bounties", UUID.randomUUID().toString() + ".bounty");
		FileConfiguration config;
		
		config = YamlConfiguration.loadConfiguration(file);
		
		config.set("Player", bounty.getPlayer());
		config.set("Amount", bounty.getAmount());
		
		try {
			config.save(file);
			this.plugin.getLogger().info("Bounty saved!");
		} catch (Exception e) {
			this.plugin.getLogger().info(e.toString());
		}
	}
}

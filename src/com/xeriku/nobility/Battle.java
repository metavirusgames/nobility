package com.xeriku.nobility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Battle {

	private String _attackedGuild;
	
	private List<String> _guilds;
	
	private Date _beginTime;
	
	private final int BATTLE_LENGTH_MINUTES = 30;
	
	public Battle(){
		this._guilds = new ArrayList<String>();
	}
	
	public void addGuild(String guild){
		
		if (!this._guilds.contains(guild)){
			_guilds.add(guild);
		}
	}
	
	public void setAttackedGuild(String guild){
		_attackedGuild = guild;
	}
	
	public void begin(){
		
	}
	
	public void end(){
		
	}
}

package com.xeriku.nobility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Guild {

	private String name;
	
	private String motto = "";
	
	private String homeCoord = "";
	
	private Map<String, Object> _players;
	
	private Map<String, Object> _landHealth;
	
	private List<String> _land;
	
	private List<String> _invites;
	
	private List<String> _allyInvites;
	
	private List<String> _neutralInvites;
	
	private int guildType;
	
	private List<String> _enemies;
	
	private List<String> _allies;
	
	private List<String> _neutrals;
	
	public Guild(){
		this._players = new HashMap<String, Object>();
		this._land = new ArrayList<String>();
		this._invites = new ArrayList<String>();
		this._allyInvites = new ArrayList<String>();
		this._neutralInvites = new ArrayList<String>();
		this._enemies = new ArrayList<String>();
		this._allies = new ArrayList<String>();
		this._neutrals = new ArrayList<String>();
		this._landHealth = new HashMap<String, Object>();
	}
	
	public void create(){
		
	}
	
	public void load(){
		//Reader input = null;
		
	}
	
	public void save(){
		

	}
	
	public void setMotto(String motto){
		this.motto = motto;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setMembers(Map<String, Object> map){
		this._players = map;
	}
	
	public void setLandHealth(Map<String, Object> map){
		this._landHealth = map;
	}
	public void setLand(List<String> map){
		
		this._land = map;
	}
	
	public void setHome(String homeCoords){
		///homeCoord = x + "," + y + "," + z;
		homeCoord = homeCoords;
	}
	
	public void setEnemies(List<String> map){
		
		if (map == null){
			map = new ArrayList<String>();
		}
		
		this._enemies = map;
	}
	
	public void setAllies(List<String> map){
		
		if (map == null){
			map = new ArrayList<String>();
		}
		
		this._allies = map;
	}
	
	public void setNeutrals(List<String> map){
		
		if (map == null){
			map = new ArrayList<String>();
		}
		
		this._neutrals = map;
	}
	
	public List<String> getEnemies(){
		
		if (this._enemies == null){
			this._enemies = new ArrayList<String>();
		}
		
		return this._enemies;
	}
	
	public List<String> getAllies(){
		
		if (this._allies == null){
			this._allies = new ArrayList<String>();
		}
		
		return this._allies;
	}
	
	public List<String> getNeutrals(){
		
		if (this._neutrals == null){
			this._neutrals = new ArrayList<String>();
		}
		
		return this._neutrals;
	}
	
	public String getHome(){
		return homeCoord;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getMotto(){
		return this.motto;
	}
	
	public Map<String, Object> getPlayers(){
		return this._players;
	}
	
	public List<String> getLand(){
		return this._land;
	}
	
	public int getLandHealth(int x, int z){
		for (Entry<String, Object> item : this._landHealth.entrySet()){
			
			if (item != null){			
				String[] coordsStrings = null;
				
				try {
					coordsStrings = item.getKey().split(",");
				} catch (Exception e) {
					//plugin.getLogger().info("There was an exception in splitting the coording strings");
					//plugin.getLogger().info(e.toString());
				}
				
				
				if (coordsStrings[0].equals(String.valueOf(x)) && coordsStrings[1].equals(String.valueOf(z))){
					return Integer.valueOf(item.getValue().toString());
				}
			}
		}
		
		return 0;
	}
	
	public Entry<String, Object> getPlayerEntry(Player player){
		
		for (Entry<String, Object> entry : this._players.entrySet()){
			
			if (entry != null){
				if (entry.getKey().equalsIgnoreCase(player.getName())){
					return entry;
				}
			}
		}
		
		return null;
	}
	
	public int getTotalAllowedClaims(){
		return this._players.size() * 30;
	}
	
	public int getTotalCurrentClaims(){
		return this._land.size();
	}
		
	public void addPlayer(Player player, Integer rank){
		this._players.put(player.getName(), rank);
	}
	
	public void claim(int x, int z){
		
		if (!ownsClaim(x, z)){
			this._land.add(x + "," + z);
					
			this._landHealth.put(x + "," + z, 30);
		}
	}
	
	public void unclaim(int x, int z){
		
		if (ownsClaim(x, z)){
			
			String coordsToUnclaim = "";
			
			for (String entry : _land){
				if (entry != null){			
					String[] coordsStrings = null;
					
					try {
						coordsStrings = entry.split(",");
					} catch (Exception e) {
						//plugin.getLogger().info("There was an exception in splitting the coording strings");
						//plugin.getLogger().info(e.toString());
					}
					
					
					if (coordsStrings[0].equals(String.valueOf(x)) && coordsStrings[1].equals(String.valueOf(z))){
						coordsToUnclaim = entry;
					}
				}
			}
			
			if (this._land.contains(coordsToUnclaim)){
				this._land.remove(coordsToUnclaim);
				
			}
			
			if (this._landHealth.containsKey(coordsToUnclaim)){
				this._landHealth.remove(coordsToUnclaim);
			}
		}
	}
	
	public void invite(String name){
		this._invites.add(name);
	}
	

	public void join(Player player){
		if (this._invites.contains(player.getName())){
			this._invites.remove(player.getName());
			addPlayer(player, RankType.Member);
		}else{
			player.sendMessage("You were not invited to this guild.");
		}
	}
	
	public void kick(String name){
		
		if (this._players.containsKey(name)){
			this._players.remove(name);
		}
	}
	
	public boolean ownsClaim(int x, int z){
		
		for (String entry : _land){
			
			if (entry != null){			
				String[] coordsStrings = null;
				
				try {
					coordsStrings = entry.split(",");
				} catch (Exception e) {
					//plugin.getLogger().info("There was an exception in splitting the coording strings");
					//plugin.getLogger().info(e.toString());
				}
				
				
				if (coordsStrings[0].equals(String.valueOf(x)) && coordsStrings[1].equals(String.valueOf(z))){
					return true;
				}
			}
		}
		
		return false;
	}
	
	public void showMap(Player player){
		Location location = player.getLocation();
		int x, y, z;
		
		x = (int)location.getX() / 16;
		y = (int)location.getY() / 256;
		z = (int)location.getZ() / 16;
		
		
		for (int zCursor = z - 4; zCursor < z + 4; zCursor++){
			String claimLine = "";
			
			for (int xCursor = x - 4; xCursor < x + 4; xCursor++){				
				if (xCursor == x && zCursor == z){
					claimLine += ChatColor.GOLD + "X" + ChatColor.GOLD;
				}else if (ownsClaim(xCursor, zCursor)){
					claimLine += ChatColor.GREEN + "+" + ChatColor.WHITE;
				}else{
					claimLine += ChatColor.GRAY + "-" + ChatColor.WHITE;
				}
			}
			
			player.sendMessage(claimLine);
		}
	}
	
	public void cancelBattles(){
		

	}
	
	public boolean isGuildInBattle(Guild otherGuild){

		return false;
	}

	public boolean isPlayerInGuild(String name){
		if (this._players.containsKey(name)){
			return true;
		}
		
		return false;
	}
	
	public boolean isPlayerOfficer(String name){
		if (this._players.containsKey(name)){
			if (this._players.get(name).toString().equalsIgnoreCase("1")){
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isPlayerGuildMaster(String name){
		if (this._players.containsKey(name)){
			if (this._players.get(name).toString().equalsIgnoreCase("2")){
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isPlayerKing(String name){
		if (this._players.containsKey(name)){
			if (this._players.get(name).toString().equalsIgnoreCase("3")){
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isGuildEnemy(String name){
		return this._enemies.contains(name);
	}
	
	public boolean isGuildAlly(String name){
		return this._allies.contains(name);
	}
	
	public boolean isGuildNeutral(String name){
		return this._neutrals.contains(name);
	}
	
	public void removePlayer(Player player){
		
	}
	
	public void ally(String name){
		if (!this._allyInvites.contains(name)){
			this._allyInvites.add(name);
		}
	}
	
	public void neutral(String name){
		if (!this._neutralInvites.contains(name)){
			this._neutralInvites.add(name);
		}
	}
	
	public void removeAllyInvite(String name){
		if (this._allyInvites.contains(name)){
			this._allyInvites.remove(name);
		}
	}
	
	public void removeNeutralInvite(String name){
		if (this._neutralInvites.contains(name)){
			this._neutralInvites.remove(name);
		}
	}
	
	public void addEnemy(String name){

		if (!this._enemies.contains(name)){
			this._enemies.add(name);
			removeAlly(name);
			removeNeutral(name);
			removeAllyInvite(name);
			removeNeutralInvite(name);
		}
	}
	
	public void addAlly(String name){
		if (!this._allies.contains(name)){
			this._allies.add(name);
			removeEnemy(name);
			removeNeutral(name);
			removeAllyInvite(name);
			removeNeutralInvite(name);
		}
	}
	
	public void addNeutral(String name){
		if (!this._neutrals.contains(name)){
			this._neutrals.add(name);
			removeEnemy(name);
			removeAlly(name);
			removeAllyInvite(name);
			removeNeutralInvite(name);
		}
	}
	
	public void removeEnemy(String name){		
		if (this._enemies.contains(name)){
			this._enemies.remove(name);
		}
	}
	
	public void removeAlly(String name){
		if (this._allies.contains(name)){
			this._allies.remove(name);
		}
	}
	
	public void removeNeutral(String name){
		if (this._neutrals.contains(name)){
			this._neutrals.remove(name);
		}
	}
	
	public List<String> getAllyInvites(){
		return this._allyInvites;
	}
	
	public List<String> getNeutralInvites(){
		return this._neutralInvites;
	}
	
	public void broadCastToGuild(NobilityPlugin plugin, String message){
		
		for (Entry<String, Object> entry : this._players.entrySet()){
			
			if (entry != null){
				Player playerToBroadcast = plugin.getServer().getPlayer(entry.getKey());
				
				if (playerToBroadcast != null && playerToBroadcast.isOnline()){
					playerToBroadcast.sendMessage(message);
				}
			}
		}
	}

	public Map<String, Object> getLandHealth() {
		return this._landHealth;
	}
}

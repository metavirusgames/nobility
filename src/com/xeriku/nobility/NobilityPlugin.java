package com.xeriku.nobility;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.*;

import com.xeriku.nobility.commands.CommandHandler;
import com.xeriku.nobility.core.BountyManager;
import com.xeriku.nobility.core.Config;
import com.xeriku.nobility.core.GuildManager;
import com.xeriku.nobility.core.Paths;
import com.xeriku.nobility.listeners.BlockChangeListener;
import com.xeriku.nobility.listeners.ChatListener;
import com.xeriku.nobility.listeners.MoveEventListener;

public final class NobilityPlugin extends JavaPlugin {

	private GuildManager guildManager;
	private BountyManager bountyManager;
	
	private ChatListener chatListener;
	private MoveEventListener moveListener;
	private BlockChangeListener blockDamageListener;
	
	private CommandHandler commandHandler;
	
	private Config config;
	
	@Override
	public void onEnable(){
		getLogger().info("Nobility has been enabled!");
		
		Paths.Check();
		
		this.config = new Config(this);
		
		this.chatListener = new ChatListener(this);
		
		this.moveListener = new MoveEventListener(this);
		
		this.blockDamageListener = new BlockChangeListener(this);
		
		this.guildManager = new GuildManager(this);
		
		this.bountyManager = new BountyManager(this);
		
		this.commandHandler = new CommandHandler(this);
		
		this.guildManager.loadGuilds();
		
		this.bountyManager.loadBounties();
	}
	
	@Override
	public void onDisable(){
		getLogger().info("Nobility has been disabled!");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		
		this.commandHandler.Handle(sender, cmd, label, args);
		
		return false;
	}

	public Config getPluginConfig(){
		return this.config;
	}
	
	public GuildManager getGuildManager(){
		return this.guildManager;
	}
	
	public BountyManager getBountyManager(){
		return this.bountyManager;
	}
	
	public ChatListener getChatListener(){
		return this.chatListener;
	}
}

package com.xeriku.nobility;

public class Bounty {

	private String playerName;
	
	private int amount;
	
	public void setPlayer(String name){
		this.playerName = name;
	}
	
	public void setAmount(int amount){
		this.amount = amount;
	}
	
	public String getPlayer(){
		return this.playerName;
	}
	
	public int getAmount(){
		return this.amount;
	}
}

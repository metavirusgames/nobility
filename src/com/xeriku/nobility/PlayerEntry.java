package com.xeriku.nobility;

import org.bukkit.entity.Player;

public class PlayerEntry {

	Player player;
	
	RankType rank;
	
	public PlayerEntry(Player player, RankType rank){
		this.player = player;
		this.rank = rank;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public RankType getRank(){
		return rank;
	}
}
